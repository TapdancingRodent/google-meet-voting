# Voting

A tool to count votes against arbitrary topics from Goole-Meet-like textual input.

## Concepts

The system takes a dictionary mapping of topics and topic aliases to an underlying topic id (see `voting_topics.yml.example`) and a dump of user votes copy pasted from Google Meet chat (usually stored in a text file to avoid whitespace and escaping issues). When a user mentions a topic name or alias in the chat, they are counted as having voted for that topic, and only the last vote of each user is counted.

## Execution

Set up topics in a yml file (see `voting_topics.yml.example` for the format) and paste the text taken from chat into a text file, then run the below command. The results will be printed to the terminal. An example vote dump can be found in `test_voting.py` if you want to test the command line functionality.

``` python
python voting.py voting_topics.yml votes.txt
```
