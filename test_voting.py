"""Tests for Google Meet vote counting tools"""
from voting import build_output, collate_voters, count_votes, load_topics, run_vote_counter, sanitise_votes

TEST_TOPICS = {
    "topic_1": "topic1",
    "topic_2": "topic2",
    "topic_1_alias": "topic1"
}

TEST_INPUT = """User14:13
Test 1
topIc_1 is the best
Test 3
User1014:21
and another test
topic_2 clearly
User014:55
topic_1_alias for sure or, maybe, Topic_1_alias
User014:56
but I am no expert
User 91214:57
topIc_1 is better than topIc_2
User14:58
I change my bote to topic_2""".split("\n")

TEST_OUTPUT = {
    "topic1": 1,
    "topic2": 3
}


def test_loaded_topics_are_dict():
    """When topics are loaded from a file
        Then a dictionary should be created"""
    assert isinstance(load_topics('voting_topics.yml.example'), dict)


def test_messages_parse():
    """When parsing message chunks
        Then individual user's messages should be chunked into groups"""
    assert next(sanitise_votes(TEST_INPUT)) == ("User", "Test 1 topIc_1 is the best Test 3")

def test_collate_voters_no_topics():
    """When parsing a user's message chunk with no topics mentioned
        Then the system should not attribute any votes to the user"""
    assert collate_voters(TEST_TOPICS, [("User", "I don't care for any topics")]) == {}


def test_collate_voters_single_topic():
    """When parsing a user's message chunk containing a single topic mention
        Then the system should attribute a mentioned topic as the user's vote"""
    assert collate_voters(TEST_TOPICS, [("User", "Test 1 topIc_1 is the best Test 3")]) == {"User": "topic1"}


def test_collate_voters_multiple_topics():
    """When parsing a user's message chunk containing multiple topic mentions
        Then the system should attribute that user's vote to the last mentioned topic"""
    assert collate_voters(TEST_TOPICS, [("User", "Test 1 topIc_1 is good but topic_2 is best")]) == {"User": "topic2"}


def test_collate_voters_multiple_votes():
    """When parsing multiple message chunks from a user containing multiple topic mentions
        Then the system should attribute that user's vote to the last mentioned topic"""
    assert collate_voters(TEST_TOPICS, [
        ("User", "Test 1 topIc_1 is good"),
        ("User", "But topic_2 is best")
        ]) == {"User": "topic2"}


def test_count_votes():
    """When counting votes
        Then system should aggregate by topic"""
    assert count_votes({
        "User": "topic1",
        "User2": "topic2",
        "User3": "topic1"
        }) == {"topic1": 2, "topic2": 1}


def test_build_output():
    """When constructing output messages
        Then the system should return a plain string with one topic per line"""
    assert build_output({"topic1": 2, "topic2": 1}) == "topic1: 2\ntopic2: 1"


def test_run_counter_empty_votes():
    """When counting votes against an empty set of topics
        Then the system should register no votes"""
    assert run_vote_counter({}, TEST_INPUT) == {}


def test_run_counter_empty_messages():
    """When counting an empty votes input against a set of topics
        Then the system should register no votes"""
    assert run_vote_counter(TEST_TOPICS, []) == {}


def test_run_counter_full():
    """When running a representative example
        Then the system should output the expected votes"""
    assert run_vote_counter(TEST_TOPICS, TEST_INPUT) == TEST_OUTPUT
