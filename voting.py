
"""Tools for capturing votes cast using Google Meet"""
from collections import defaultdict
import re
import sys
import yaml


def load_topics(topic_file):
    """Load a topics config file"""
    with open(topic_file) as yml_file:
        topic_dict = yaml.load(yml_file, Loader=yaml.FullLoader)["topics"]

    for topic_ref, topic_name in topic_dict.items():
        topic_dict[topic_ref.lower()] = topic_name

    return topic_dict


def load_votes(votes_file):
    """Load votes from a file"""
    with open(votes_file) as txt_file:
        return txt_file.readlines()


def sanitise_votes(vote_lines):
    """Chunk votes up into user messages so that votes are attributable"""
    messages = []
    message_buffer = []
    user_regex = re.compile(r"^([\s\w]*)[\d]{2}:[\d]{2}$")
    for chunk in reversed(vote_lines):
        user_match = user_regex.match(chunk)
        if user_match:
            messages.append((user_match.groups()[0], ' '.join(reversed(message_buffer))))
            message_buffer = []
        else:
            message_buffer.append(chunk)

    return reversed(messages)


def collate_voters(topic_dict, messages):
    """Collate voters' entries so that there is only 1 vote per voter"""
    topic_regex = re.compile(f"({'|'.join(topic_dict.keys())})", re.IGNORECASE)
    votes = {}
    for user, message in messages:
        topic_mentions = topic_regex.findall(message)
        if topic_mentions:
            votes[user] = topic_dict[topic_mentions[-1].lower()]

    return votes


def count_votes(user_votes):
    """Count users' votes by topic"""
    topic_votes = defaultdict(int)
    for topic in user_votes.values():
        topic_votes[topic] += 1

    return topic_votes


def run_vote_counter(topic_dict, vote_lines):
    """Count votes against a list of topics in a Google Meet-like chat dump"""
    if not topic_dict:
        return {}

    messages = sanitise_votes(vote_lines)
    user_votes = collate_voters(topic_dict, messages)

    return count_votes(user_votes)


def build_output(topic_votes):
    """Construct an output string for command line run mode"""
    output_buffer = []
    for key, value in sorted(topic_votes.items(), key=lambda item: item[0]):
        output_buffer.append(f"{key}: {value}")

    return "\n".join(output_buffer)


def run_from_command_line(topic_file, vote_file):
    """Run a vote counter from the command line and output the results"""
    topic_dict = load_topics(topic_file)
    vote_lines = load_votes(vote_file)
    votes_list = run_vote_counter(topic_dict, vote_lines)

    print(build_output(votes_list))


if __name__ == "__main__":
    run_from_command_line(sys.argv[1], sys.argv[2])
